// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { loremIpsum } from "lorem-ipsum";

type Article = {
  title: string;
  image: string;
  author: string;
  published: number;
  link: string;
};

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Array<Article>>
) {
  const count = req.query.v || 30;
  const list = [];

  for (let idx=0; idx<count; idx++)
  {
    list.push( generateArticle(idx) );
  }

  res.status(200).json(list);
}

function generateArticle(id: number): Article {
    const title = loremIpsum({
        count: 1,
        format: "plain",
        random: Math.random,
        sentenceLowerBound: 7,
        sentenceUpperBound: 12,
        units: "sentences",
    });

    // Some random date within the last 5 days
    const date = Math.floor(Date.now() / 1000) - Math.floor( Math.random() * (60 * 60 * 24 * 5) );

    return {
        title: title,
        image: 'https://picsum.photos/1280/720',
        author: 'First Lastname',
        published: date,
        link: '/test/' + id,
    };
}