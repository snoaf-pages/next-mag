import type { NextPage } from 'next'

import Head from 'next/head'
import Header from '../components/Header'
import ArticleList from '../components/ArticleList'
import Image from 'next/image'
import styles from '../styles/Home.module.scss'
import ArticleListData from '../public/article-list.json'

const Home: NextPage = () => {
  return (
    <div className={styles.container}>
      <Head>
        <title>Online Mag</title>
        <meta name="description" content="The Next Online Magazine" />
      </Head>

      <Header/>

      <main>
        <ArticleList
          articleList={ ArticleListData }
        />
      </main>

      <footer className={styles.footer}>
        <p>
          Page built by <a href="mailto:contact+tis@snoaf.com">Tyson</a>
        </p>
      </footer>
    </div>
  )
}

export default Home
