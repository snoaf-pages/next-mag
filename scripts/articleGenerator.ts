const path = require('path');
const fs = require('fs');
const loremIpsum = require('lorem-ipsum').loremIpsum;

type Article = {
    title: string;
    image: string;
    author: string;
    published: number;
    description: string;
    body: string;
    link?: string;
};

const listFilePath = path.join(__dirname, "../public/article-list.json");
const articleFilePath = path.join(__dirname, "../public/articles");

const main = async () => {
    const count = 30;
    const list = [];

    try {
        if (!fs.existsSync(articleFilePath))
        {
            fs.rmSync( articleFilePath, { recursive: true, force: true } );
        }
        fs.mkdirSync( articleFilePath, { recursive: true } );
    } catch (err) {
        console.error(`Error while deleting/recreating ${articleFilePath}.`);
    }

    for (let idx=0; idx<count; idx++)
    {
        const article = generateArticle();
        const articleDate = new Date(article.published * 1000);
        const dateString = `${articleDate.getUTCFullYear()}-${articleDate.getUTCMonth() + 1}-${articleDate.getUTCDate()}`;
        const urlTitle = article.title.toLowerCase().split(' ').slice(0, 5).join('-');
        const location = `/articles/${dateString}-${urlTitle}`;
        const articleFilePath = path.join(__dirname, "../public/", location + '.json');
        
        list.push( {
            title: article.title,
            image: article.image,
            author: article.author,
            published: article.published,
            link: location,
        } );

        fs.writeFileSync(articleFilePath, JSON.stringify(article, null, 2));
    }

    list.sort( (articleA, articleB) => {
        return articleA.published - articleB.published;
    } );

    fs.writeFileSync(listFilePath, JSON.stringify(list, null, 2));
};

main().then(() => console.log("Done."));

function generateArticle(): Article {
    const title = loremIpsum({
        count: 1,
        format: "plain",
        random: Math.random,
        sentenceLowerBound: 7,
        sentenceUpperBound: 11,
        units: "sentences",
    });

    const description = loremIpsum({
        count: 1,
        format: "plain",
        random: Math.random,
        sentenceLowerBound: 10,
        sentenceUpperBound: 20,
        units: "sentences",
    });
    
    const article = loremIpsum({
        count: 8,
        format: "html",
        paragraphLowerBound: 3,
        paragraphUpperBound: 7,
        random: Math.random,
        sentenceLowerBound: 5,
        sentenceUpperBound: 15,
        units: "paragraphs",
    });

    // Some random date within the last 5 days
    const date = Math.floor(Date.now() / 1000) - Math.floor( Math.random() * (60 * 60 * 24 * 5) );

    return {
        title: title.replaceAll('.', ''),
        image: `https://picsum.photos/id/${Math.floor( Math.random() * 1080 )}/1280/720`,
        author: 'First Lastname',
        published: date,
        description: description,
        body: article
    };
}