import React from 'react';
import styles from '../styles/ArticleList.module.scss'

type Article = {
    title: string;
    image: string;
    author: string;
    published: number;
    link: string;
};

type ArticleListProps = {
    articleList: Array<Article>;
};

enum ArticleStyle{
    highlight1,
    highlight2,
    highlight3,
    normal
};

class ArticleList extends React.Component< ArticleListProps > {
    constructor (props: ArticleListProps) {
        super(props);
    }

    createArticleLink(article: Article, style: ArticleStyle, hasKey?: boolean): React.ReactNode {
        // Note: feeding the enum back into its type gives the string
        const articleStyle = styles[ ArticleStyle[style] ];
        
        return (
            <div className={ articleStyle } key={hasKey ? article.published : ''}>
                <a href={ article.link }>
                    <span className={ styles.imgContainer }>
                        <img src={ article.image } alt="" />
                    </span>
                    <span className={styles.data}>
                        <h2 className={styles.title}>{ article.title }</h2>
                        <span className={styles.author}>{ article.author }</span>
                    </span>
                </a>
            </div>
        );
    }

    render(): React.ReactNode {
        const articleList = this.props.articleList.slice();
        const articleLinksList = [];

        for (let idx=0; idx < articleList.length; idx++)
        {
            let linkStyle: ArticleStyle = idx < 3 ? idx : ArticleStyle.normal;
            
            articleLinksList.push( this.createArticleLink(articleList[idx], linkStyle, true) );
        }

        return (
            <div className={styles.articleList}>
                { articleLinksList }
            </div>
        );
    }
}

export default ArticleList;