import React from 'react';
import styles from '../styles/Header.module.scss'

class Header extends React.Component {

  nav(): React.ReactNode {
    return (
      <div>
        <button className={styles.menuButton}>Menu</button>
        <nav className={styles.nav}>
          <a href="#">Generate Page</a>
          <a href="#">About</a>
          <a href="#">Contact</a>
          <button className={styles.searchButton}>Search</button>
        </nav>
      </div>
    );
  }

  render(): React.ReactNode {
    return (
      <header className={styles.header}>
        <h1 className={styles.title}>
            Online Magazine
        </h1>

        { this.nav() }
      </header>
    );
  }
}



export default Header